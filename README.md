# ttl-dashboard-shiny

## Tempe Town Lake water quality and chlorophyll

Data viewer for long-term, high-resolution water quality and chlorophyll data as measured by the CAP LTER using a Eureka Manta+35 multirobe datasonde.

build and run sensu:

```r
shinylive::export(
  appdir = "~/localRepos/ttl-dashboard-shiny/",
  destdir = "~/Desktop/ttl-live-base/"
)

shinylive::export(
  appdir = "ttl/",
  destdir = "public/"
)

httpuv::runStaticServer(
  dir = "ttl-live-base/",
  port = 8888
)
```
